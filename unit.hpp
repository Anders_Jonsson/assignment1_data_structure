// unit.hpp

#pragma once

#include <iostream>
#include <string>
#include "linkedList.hpp"

template <class T>
bool are_equal(T a, T b)
{
	return a == b;
}

template <class T>
bool verify(T expected, T got, const std::string& message)
{
	if (are_equal(expected, got))
	{
		std::cout << "Passed: " << message << std::endl;
		return true;
	}
	std::cout << "Failed! Expected: " << expected << " Got: " << got <<
		". Function: " << message << std::endl;
	return false;
}

template <class T>
bool verifyList(LinkedList<T> *expected, LinkedList<T> *got, const std::string& message)
{
	for (int i = 0; i < expected->Size(); i++)
	{
		if (!are_equal(expected->find_at(i), got->find_at(i)))
		{
			std::cout << "Failed! Expected: " << expected->find_at(i) << " Got: " << got->find_at(i) <<
				". Function: " << message << std::endl;
			return false;
		}
	}
	std::cout << "Passed: " << message << std::endl;
	return true;
}