// linkedLists.hpp/

#pragma once

template <class T>
class LinkedList
{


	struct Node
	{
		T value;
		Node* prev;
		Node* next;
		
	};

	//private:
	Node* base;
	Node* end;
	Node* temp;
	Node* curr;

public:
	LinkedList();
	

	~LinkedList();
	


	void PushFront(T value);
	void PushBack(T value);
	void PopFront();
	void PopBack();
	void Clear();
	int Find(T value);
	T find_at(int index);
	
	int Size(); 
	T Base();
	T End();

};
	template <class T>

	void LinkedList<T>::PushFront(T value)
	{
		Node *node = new Node;
		node->value = value;
		node->next = nullptr;

		if (base != nullptr)
		{
			temp = base;
			base = node;
			base->next = temp;
		
		}
		else
			base = node;

		
	};
	template <class T>

	void LinkedList<T>::PushBack(T value)
	{
		Node *node = new Node;
		node->value = value;
		node->next = nullptr;

		if (base)
		{
			curr = base;
			while (curr->next != nullptr)
			{
				curr = curr->next;
			}
			curr->next = node;
		}
		else
			base = node;
		
		

	};
	template <class T>

	void LinkedList<T>::PopFront()
	{
		if (base->next != nullptr)
		{
			Node* tempNode = base->next;
			base->next->prev = nullptr;
			delete base;
			base = tempNode;

		}
		else
			delete base;

	};

	template <class T>

	void LinkedList<T>::PopBack()
	{
		temp = base;
		while (temp)
		{
			if (temp->next->next == nullptr)
			{
				delete temp->next;
				temp->next = nullptr;
				break;
			}
			temp = temp->next;
		}
	};

	template <class T>

	void LinkedList<T>::Clear()
	{
		if (base)
		{
			temp = base;
			while (temp)
			{
				Node* node = temp->next;
				delete temp;
				temp = nullptr;
				temp = node;
			}
			
			base = nullptr;
			
		}
		


	};
	template <class T>


	int LinkedList<T>::Find(T value)
	{

		Node* i = base;
		int count = 0;
		
		while (i != nullptr)
		{
			if (i->value == value)
				return count;
			else
			{
				i = i->next;
				count++;
			}
		}
		
		return -1;

	};

	template <class T>
	T LinkedList<T>::find_at(int index)
	{
		if (base)
		{
			int i = 0;
			temp = base;
			while (temp)
			{
				if (i == index)
					return temp->value;
				else
				{
					temp = temp->next;
					i++;

				}
			}

		}
		else
			return 0;

		return 0;
	}


	template <class T>

	int LinkedList<T>::Size()
	{
		int count = 0;
		if (base)
		{
			Node *node = base;
			
			while (node)
			{
				count++;
				node = node->next;
			}
			return count;
		}
		else
			return 0;

		return 0;
	};

	template <class T>

	T LinkedList<T>::Base()
	{
		if (base)
			return base->value;
		else
			return 0;

	};

	template <class T>

	T LinkedList<T>::End()
	{

		temp = base;
		while (temp)
		{
			if (temp->next)
				temp = temp->next;
			else if (!temp->next)
					return temp->value;
		}
		if (!base)
			return 0;

		return 0;
	};

	template <class T>

	LinkedList<T>::LinkedList()
	{
		base = nullptr;
	}
	template <class T>

	LinkedList<T>::~LinkedList()
	{
		Clear();
	}
