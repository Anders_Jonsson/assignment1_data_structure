// binaryTree.hpp/

#pragma once
#include <iostream>
#include "linkedList.hpp"

template <class T>
class BinaryTree
{


	struct Node
	{
		T value;
		Node* high;
		Node* low;

	};

	//private:
	Node* base;
	Node* checkNode;
	Node* temp;
	Node* curr;
	Node* node;

public:
	BinaryTree();


	~BinaryTree();

	void Insert(T value);
	bool Find(T value);
	int  Size();
	void Size_rec(Node * node, int& size);
	void Clear();
	void Clear_rec(Node** node);

	LinkedList<T>* traversal_pre_order();
	LinkedList<T>* traversal_in_order();
	LinkedList<T>* traversal_post_order();

	void print_pre_order_rec(Node* node, LinkedList<T>& list);
	void print_in_order_rec(Node* node, LinkedList<T>& list);
	void print_post_order_rec(Node* node, LinkedList<T>& list);

	LinkedList<T> *preOrder;
	LinkedList<T> *inOrder;
	LinkedList<T> *postOrder;


};

template <class T>
BinaryTree<T>::BinaryTree()
	:base(nullptr),
	temp(nullptr),
	checkNode(nullptr)
{
	preOrder = new LinkedList<T>;
	inOrder = new LinkedList<T>;
	postOrder = new LinkedList<T>;
	 
}

template <class T>
BinaryTree<T>::~BinaryTree()
{
	Clear();
	

	delete preOrder;
	preOrder = nullptr;

	delete inOrder;
	inOrder = nullptr;

	delete postOrder;
	postOrder = nullptr;
}

template <class T>
void BinaryTree<T>::Insert(T value)
{
	node = new Node;
	node->value = value;
	node->high = nullptr;
	node->low = nullptr;

	if (base)
	{
		temp = base;
		while (temp)
		{
			if (value > temp->value)
			{
				if (temp->high == nullptr)
				{
					temp->high = node;
					break;
				}
				else
					temp = temp->high;
			}
			else
			{
				if (temp->low == nullptr)
				{
					temp->low = node;
					break;
				}
				else
					temp = temp->low;
			}
		}
	}
	else
		base = node;
	/*Node* tempNode = new Node;
	tempNode->value = value;
   

	if (base)
	{


		  checkNode = base;
		while (base)
		{
			if (value > checkNode->value)
			{
				if (checkNode->high == nullptr)
				{
					checkNode->high = tempNode;
					return;
				}
				else
				{
					checkNode = checkNode->high;

				}

			}
			else if (value < checkNode->value)
			{
				if (checkNode->low == nullptr)
				{
					checkNode->low = tempNode;
					return;
				}
				else
				{
					checkNode = checkNode->low;

				}

			}

		}
	}
	else
	{
		base = tempNode;

		return;
	}*/

	

}

template <class T>
bool BinaryTree<T>::Find(T value)
{

	if (base != nullptr)
	{
		Node* tempNode = base;
		while (tempNode != nullptr)
		{
			if (value == tempNode->value)
			{
				return true;
			}
			else if (value > tempNode->value)
			{
				tempNode = tempNode->high;
			}
			else if (value < tempNode->value)
			{
				tempNode = tempNode->low;
			}

		}
		return false;

	}
	else
		return false;

}

template <class T>
int BinaryTree<T>::Size()
{
	int size = 0;
	Size_rec(base, size);

	return size;
}

template <class T>
void BinaryTree<T>::Size_rec(Node* node, int &size)
{
	if (!node)
		return;
	Size_rec(node->high, size);
	Size_rec(node->low, size);
	size++;
}

template <class T>
void BinaryTree<T>::Clear()
{
	Clear_rec(&base);


}

template <class T>
void BinaryTree<T>::Clear_rec(Node** node)
{
	if ((*node) == nullptr)
		return;

	Clear_rec(&(*node)->low);
	Clear_rec(&(*node)->high);

	if (!((*node)->low) && !((*node)->high))
	{
		delete ((*node));
		*node = nullptr;
	}

}

template <class T>
LinkedList<T>* BinaryTree<T>::traversal_pre_order()
{
	if (base)
	{
		print_pre_order_rec(base, *preOrder);
		return preOrder;
	}
	//maybe do something more?
	return preOrder;
}

template <class T>
LinkedList<T>* BinaryTree<T>::traversal_in_order()
{
	if (base)
	{
		print_in_order_rec(base, *inOrder);
		return inOrder;
	}
	//more?
	return inOrder;
}

template <class T>
LinkedList<T>* BinaryTree<T>::traversal_post_order()
{
	if (base)
	{
		print_post_order_rec(base, *postOrder);
		return postOrder;
	}
	//more?
	return postOrder;
}

template <class T>
void BinaryTree<T>::print_pre_order_rec(Node* node, LinkedList<T>& list)
{
	if (!node)
		return;

	list.PushBack(node->value);
	print_pre_order_rec(node->low, list);
	print_pre_order_rec(node->high, list);
}

template <class T>
void BinaryTree<T>::print_in_order_rec(Node* node, LinkedList<T>& list)
{
	if (!node)
		return;

	    print_in_order_rec(node->low, list);
		list.PushBack(node->value);
		print_in_order_rec(node->high, list);
}

template <class T>
void BinaryTree<T>::print_post_order_rec(Node* node, LinkedList<T>& list)
{
	if (!node)
		return;
	print_post_order_rec(node->low, list);
	print_post_order_rec(node->high, list);
	list.PushBack(node->value);

}

