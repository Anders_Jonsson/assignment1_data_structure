	//main.cpp

#include "linkedList.hpp"
#include "binaryTree.hpp"
#include <stdio.h>
#include <tchar.h>
#include <iostream>
#include <vld.h>

#include "unit.hpp"


template<class T>
void UnitTestingLinkedList(LinkedList<T> list);
template<class T>
void UnitTestingBinarySearchTree(BinaryTree<T> tree);


int main()
{

	LinkedList<int> list;
	BinaryTree<int> tree;


	list.PushBack(4);
	list.PushBack(424);
	list.PushFront(56);
	list.PushBack(2);
	list.PushBack(85);
	list.PushFront(32);
	list.PushFront(-46);
	list.PushBack(17);
	list.PushBack(46);
	list.PushFront(75);
	list.PushBack(-9999);
	list.PushFront(745);
	list.PushBack(10);
	list.PushBack(3);
	list.PushFront(-18188);

	tree.Insert(4);
	tree.Insert(424);
	tree.Insert(56);
	tree.Insert(2);
	tree.Insert(85);
	tree.Insert(32);
	tree.Insert(-46);
	tree.Insert(17);
	tree.Insert(46);
	tree.Insert(75);
	tree.Insert(-9999);
	tree.Insert(745);
	tree.Insert(10);
	tree.Insert(3);
	tree.Insert(-181818);

	
	UnitTestingLinkedList(list);
	std::cout << "" << std::endl;
	UnitTestingBinarySearchTree(tree);

	
	

	
	char input;
	std::cin >> input;

	

	return 0;
};

template<class T>
void UnitTestingLinkedList(LinkedList<T> list)
{
	std::cout << " " << std::endl;

	//Size()
	verify<int>(15, list.Size(), "Size()");

	//PushFront()
	list.PushFront(20);
	verify<int>(20, list.Base(), "PushFront()");

	//PushBack()
	list.PushBack(33);
	verify<int>(33, list.End(), "PushBack()");

	list.PushBack(15);
	//PopBack()
	list.PopBack();
	verify<int>(33, list.End(), "PopBack()");
	
	list.PushFront(15);
	//PopFront()
	list.PopFront();
	verify<int>(20, list.Base(), "PopFront()");

	//Clear()
	list.Clear();
	verify<int>(0, list.Size(), "clear()");
}


template<class T>
void UnitTestingBinarySearchTree(BinaryTree<T> list)
{

	LinkedList<int> *preOrder = new LinkedList<int>;
	LinkedList<int> *inOrder = new LinkedList<int>;
	LinkedList<int> *postOrder = new LinkedList<int>;

	for (int i = 0; i < list.Size(); i++)
	{
		preOrder->PushBack(list.traversal_pre_order()->find_at(i));
	}

	for (int i = 0; i < list.Size(); i++)
	{
		inOrder->PushBack(list.traversal_in_order()->find_at(i));
	}

	for (int i = 0; i < list.Size(); i++)
	{
		postOrder->PushBack(list.traversal_post_order()->find_at(i));
		
	}

	//size()
	verify<int>(15, list.Size(), "size()");

	//find()
	verify<int>(1, list.Find(56), "find()");

	//traversal_pre_order
	verifyList<int>(preOrder, list.traversal_pre_order(), "traversal_pre_order()");

	//traversal_in_order
	verifyList<int>(inOrder, list.traversal_in_order(), "traversal_in_order()");

	//traversal_post_order
	verifyList<int>(postOrder, list.traversal_post_order(), "traversal_pre_order()");
	//Clear()
	list.Clear();
	verify<int>(0, list.Size(), "Clear()");

	delete preOrder;
	preOrder = nullptr;
	delete inOrder;
	inOrder = nullptr;
	delete postOrder;
	postOrder = nullptr;

}